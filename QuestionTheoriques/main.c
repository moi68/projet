#include <stdio.h>
#include <stdlib.h>

int main()
{
    int nb = 10;

    int i = 1, j = 1;
    for(i =1; i <= nb; i++) //on commence � 1 pour la boucle for
    {
        printf("%d\n",i);

    }
    printf("\n\n");

    while(j <= nb) //on commence � 1 pour la boucle while
    {
        printf("%d\n",j);
        j++;
    }

    printf("\n\n");

    int choix;
    printf("\n\n===Saisir une valeur entre 1 et 3\n"); // on demande de saisir un choix
    scanf("%d", &choix); // on valide le choix afin de l'afficher par la suite
    if(choix == 1)
    {
        printf("Vous avez demandez un gateau au chocolat\n");
    }
    else if(choix == 2 || choix == 3)
    {
        printf("Vous avez demandez une tarte au citron et un flan\n");
    }

    else
    {
        printf("Vous n'avez rien pris !\n");
    }

    printf("\n\n===Saisir une valeur entre 1 et 3\n");// on redemande un choix
    scanf("%d", &choix); // on valide le choix
    switch(choix)
    {
    case 1:
        printf("Vous avez demandez un gateau au chocolat\n");
        break;
    case 2:
    case 3:
        printf("Vous avez demandez une tarte au citron et un flan\n");
        break;
    default:
        printf("Vous n'avez rien pris !\n");
        break;
    }
    return 0;
}
/*
Diif�rence entre un #define et une constante :
#define : d�clarer en dehors du 'main', permet au programmeur de cr�er un nom pour une constante et de l'utiliser tout au long
du programme. Si la constante doit �tre modifi� au sein du programme, il suffit de la modifier une seule fois dans la directive
de pr�compilation #define; lorsque le programme est recompil�, toutes les occurences de la constante sont automatiquement modifi�s.
Donc tout ce qui est � droite du  #define  est remplacer si on le modifie, si on a #define x=515, cela entraine le remplacement de
chaque occurence de x par 515.

une constante: d�clarer en dehors du 'main' et a un type de donn�e sp�cifique contrairement au 'define'.
La constante est visible par son nom pour un d�bogueur. L'inconvienent des variables constante est qu'elles peuvent exiger
un emplacement m�moire de la taille de leur type de donn�e, tandis qu'avec le "define", cela ne requiert pas de m�moire
suppl�mentaire.
*/
